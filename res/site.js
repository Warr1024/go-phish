var app = angular.module("app", ["ngRoute", "ngAria", "ngTouch"]);

app.config(["$routeProvider", function($routeProvider) {
	$routeProvider
		.when("/:id*", {
			template: "<phished>"
		})
		.when("/", {
			template: "<approot>"
		})
		.otherwise({
			redirectTo: "/"
		});
}]);

app.directive("approot", [function() {
	return {
		templateUrl: "view-approot.html",
		scope: {},
		controller: ["$scope", "$http", function($scope, $http) {
			$scope.reload = function() {
				$http.get("players.json").then(function(x) {
					$scope.players = x.data;
					$scope.names = {};
					for(var i = 0; i < x.data.length; i++) {
						$scope.names[x.data[i].id] = x.data[i].name
							|| x.data[i].id;
						if(x.data[i].me) {
							$scope.me = {id: x.data[i].id};
							$scope.rename.to = $scope.names[$scope.me.id];
						}
					}
				});
				$http.get("log.json").then(function(x) { $scope.log = x.data; });
			};
			$scope.rename = function() {
				$http.post("rename/" + escape($scope.rename.to)).then(function(x) {
					if(x.data && x.data.err)
						alert("Error: " + x.data.err);
					if(x.data && x.data.ok) {
						$scope.reload();
						alert("Success!");
					}
				});
			};
			$scope.reload();
		}]
	};
}]);

app.directive("phished", [function() {
	return {
		templateUrl: "view-phished.html",
		scope: {},
		controller: ["$scope", "$http", "$location", "$routeParams",
		  function($scope, $http, $location, $routeParams) {
			$http.post("kill/" + $routeParams.id).then(function(x) {
				$scope.err = x.data.err;
				if(x.data.ok)
					$location.path("/");
			});
		}]
	};
}]);
