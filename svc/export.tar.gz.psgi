sub {
	open(my $fh, "-|", qw(tar czf - .));
	return [200, ["Content-Type" => "application/tar+gzip"], [do { local $/; <$fh> }]];
}
