sub {
	my $env = shift();
	my $p = playerid($env);
	return sql {
		my @s = @{sqlsel("select stamp, loseid, campaign from kills where winid = ?"
		  . " order by stamp desc limit 100", $p)};
		push @s, @{sqlsel("select stamp, winid from kills where loseid = ?"
		  . " and winid != ? order by stamp desc limit 100", $p, $p)};
		@s = grep { $_ } (sort { $b->{stamp} <=> $a->{stamp} } @s)[0..100];
		return jsonresp([@s]);
	};
};
