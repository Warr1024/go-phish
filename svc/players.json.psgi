sub {
	my $env = shift();
	my $p = playerid($env);
	return sql {
		my @s = @{sqlsel("select id, name, score, kills, deaths from players order by score desc")};
		for my $i ( @s ) { $i->{id} eq $p and $i->{me} = 1; }
		return jsonresp([@s]);
	};
};
