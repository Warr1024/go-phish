sub {
	my $env = shift();

	$env->{REQUEST_METHOD} eq "POST" or return jsonresp({err => " HTTP POST only"});

	$env->{PATH_INFO} =~ m#^/*\s*(\S.*)#
	  or return jsonresp({err => "no name"});
	my $n = $1;

	my $p = playerid($env);

	sql {
		@{sqlsel("select name from players where name = ?", $n)}
		  and return jsonresp({err => "name already in use"});

		sqldo("update players set name = ? where id = ?", $n, $p);
		return jsonresp({ok => 1});
	};
};
