use Time::HiRes qw(time);

sub {
	my $env = shift();

	$env->{REQUEST_METHOD} eq "POST" or return jsonresp({err => " HTTP POST only"});

	$env->{PATH_INFO} =~ m#^/*([^/]+)(?:/(.*))?#
	  or return jsonresp({err => "no phisher id"});
	my($win, $camp) = ($1, $2);

	my $p = playerid($env);
	$p eq $win and return jsonresp({err => "cannot self-phish"});

	return sql {
		my $n = int(time() * 1000);
		my $t = @{sqlsel("select max(stamp) as max from kills where loseid = ?", $p)
		  || {max => 0}}[0]->{max};
		$t > ($n - 60000) and return jsonresp({err => "respawn immunity"});

		@{sqlsel("select id from players where id = ?", $win)}
		  or return jsonresp({err => "phisher id not found"});

		sqldo("insert into kills(winid, loseid, stamp, campaign) values(?, ?, ?, ?)",
		  $win, $p, $n, $camp);
		sqldo("update players set kills = kills + 1, score = score + 1 where id = ?", $win);
		sqldo("update players set deaths = deaths + 1, score = score - 1 where id = ?", $p);

		return jsonresp({ok => 1});
	};
};
