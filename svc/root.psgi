use File::MimeInfo qw(mimetype);

sub {
	my $env = shift();

	my @p = grep { m#\S# } split(m#/#, $env->{PATH_INFO});
	grep { m#[^A-Za-z0-9\._-]# or m#^\.# } @p and die("invalid path");
	@p or push @p, "index.html";
	unshift @p, "res";
	my $f = join("/", @p);
	-e $f or return [404, ["Content-Type" => "text/plain"], ["Not found."]];

	my $m = mimetype($f);

	open(my $fh, "<", $f) or die($!);
	my $x = do { local $/; <$fh> };
	close($fh);

	return [200, ["Content-Type" => $m], [$x]];
};
