#!/usr/bin/env plackup
use strict;
use warnings;
use Plack::Builder;
use DBI qw();
use DBD::SQLite qw();
use JSON qw(from_json to_json);

# NOTE: When hosting under nginx, the URI must be correctly split between
# SCRIPT_FILENAME and PATH_INFO, with the psgi app root as SCRIPT_FILENAME
# and all the rest as PATH_INFO.
# To do this, set "fastcgi_split_path_info" to a regex that captures the
# static part of the path first, then the dynamic part that goes into PATH_INFO.

# ----------------------------------------------------------------------
# DATABASE

my $initsql = <<'EOD';

	create table if not exists players(
		id, addr, name, kills default 0, deaths default 0, score default 0);
	create unique index if not exists players_addr on players(addr);

	create table if not exists kills(
		stamp, winid, loseid, campaign);
	create index if not exists kills_lose_stamp on kills(loseid, stamp);
	create index if not exists kills_win_stamp on kills(winid, stamp);

EOD

my $dbpath = "phished.sqlite";
our $db;
sub sql(&) {
        my($act) = @_;

        my $dbh = DBI->connect(
                "dbi:SQLite:dbname=$dbpath",
                "", "", {RaiseError => 1, AutoCommit => 0,
		sqlite_allow_multiple_statements => 1}
        ) or die("failed to connect to db");

        local $@;
        my $r;
        eval {
                local $db = $dbh;
                $act and $r = $act->();
                $dbh->commit();
        };
        my @err;
        $@ and push @err, $@;

        eval {
                $dbh->rollback();
                $dbh->disconnect();
        };
        $@ and push @err, $@;

        scalar(@err) and die(join("", @err));
        return $r;
}

sub sqlsel { $db->selectall_arrayref(shift(@_), {Slice=>{}}, @_) }

sub sqldo { $db->do(shift(@_), {}, @_) }

sql { sqldo($initsql) };

# ----------------------------------------------------------------------
# PLAYER IDENTIFICATION

sub playerid {
	my $env = shift();
	my $addr = $env->{REMOTE_ADDR};
	return sql {
		my @q = @{sqlsel("select id from players where addr = ?", $addr)};
		@q and return $q[0]->{id};

		my $playid = join("", map { substr("abcdefghijklmnopqrstuvqxyz",
		  rand(26), 1) } 1..12);
		sqldo("insert into players(id, addr) values(?, ?)", $playid, $addr);
		return $playid;
	};
}

# ----------------------------------------------------------------------
# REQUEST HANDLERS

sub jsonresp {
	my $j = to_json(shift(), { map { $_ => 1 } qw(pretty canonical
		allow_blessed convert_blessed allow_unknown) });
	#[200, ["Content-Type" => "application/json"], [")]}',\n", $j]];
	[200, ["Content-Type" => "application/json"], [$j]];
}

my %mounts;
opendir(my $dh, "svc") or die($!);
while(my $e = readdir($dh)) {
	$e =~ m#^\.# and next;
	my $f = "svc/$e";
	$e =~ s#\.psgi$## or next;
	-d $f and next;
	$e eq "root" and $e = "";
	$mounts{"/$e"} = $f;
}
closedir($dh);

my $mnteval = sub {
	my $e = shift();
	my $f = $mounts{$e};
	ref $f eq "CODE" and return $f->(@_);
	$f = do $f;
	$@ and die $@;
	ref $f eq "CODE" or die("$e => $f not a CODE ref");
	$mounts{$e} = $f;
	return $f->(@_);
};

eval "builder { " . join(", ", map { "mount \"$_\" => sub { \$mnteval->(\"$_\", \@_); }" } keys %mounts) . " };";
