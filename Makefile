test:
	plackup -L Shotgun --host 0.0.0.0

host:
	mkdir -p /var/www/tmp
	spawn-fcgi -n -s /var/www/tmp/$U.sock -M 0600 -u $U -U nginx \
	  -- /usr/bin/env plackup -s FCGI --nproc 2 --proc-title $U app.psgi

prod:
	env U=phished make host

qa:
	env U=phishedqa make host
